<h1>Door Lock 9000</h1>

Arduino Powered Keypad Door Lock<br>
By: Joseph Gambino<br>
	<gambino@csh.rit.edu><br>
	<https://github.com/joegiants182><br>
	<https://github.com/joegiants182/doorlock9000><br>

<h2>Parts:</h2>

	Arduino (Micro/Uno reccomended, any will do however)
	Keypad
	4 560 Ohm resistors
	4 LEDs
	Servo (Make sure it is powerful enough to turn the lock, different doors may vary)
	Breadboard/Perf-board
	Solder/Soldering Iron (if using perf-board)
	Wires
	Most of these parts can be found on http://www.adafruit.com/

<h2>Setup:</h2>

	1. Construct the circuit in lockcircuit.png using either perf-board or a breadboard
		Note: Using the Arduino micro and the same pins as shown in lockcircuit.png will
		allow the code to work as-is.  However, using any type of Arduino will work.

	2. If you used a pin layout different then the one shown in the diagram, change the 
	   pin numbers in the top of the code to match your setup.

	3. Set the code variable to your desired passcode in this format: {0,0,0,0}

	4. Upload the code to the arduino

	5. Test your connections and make sure the circuit works

	6. Build some type of bracket for your servo, and a wooden piece to go around the lock
	   knob, cut an indent in the side so that the wood sits flush with the door when over
	   the lock.

	7. Mount the servo, feed the wires for the keypad through the eyehole or around the
	   door frame.  Mount the breadboard/perf-board to the door, and connect all the wires.

	8. Test the passcode entry, and ensure all is setup properly, and enjoy key-less entry
	   into your room!