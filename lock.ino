#include <Servo.h>
#include <Keypad.h>

#define IRpin_PIN PIND //sets the IRpin_PIN to reference the status of PIND

const byte ROWS = 4; //number of rows on keypad
const byte COLS = 3; //number of columns on keypad
//defines output given when each key is pressed
char keys[ROWS][COLS] = 
{
  {1,2,3},
  {4,5,6},
  {7,8,9},
  {'*',10,'#'}
};

//pin setup
int IRpin = 13; //IR pin number
int servoPin = 1; //servo pin number
int ledPins[] = {9,10,11,12}; //LED pin numbers
byte rowPins[ROWS] = {8, 7, 6, 5}; //keypad row pins
byte colPins[COLS] = {4, 3, 2}; //keypad column pins

Servo doorLock; //creates servo object for door lock
Keypad keypad = Keypad( makeKeymap(keys), rowPins, colPins, ROWS, COLS ); //create keypad object

int unlockPos = 150; //servo position when door is unlocked
int lockPos = 40; //servo position when door is locked
boolean locked = false; //boolean to tell if door is locked

int code[] ={1,2,3,4}; //defines 4 digit passcode
int entry[] = {10,10,10,10}; //stores user input for comparison against code
int curr = 0; //current number of key presses




void setup() //runs when the arduino first starts
{

  doorLock.attach(servoPin); //attaches the servo to the doorLock object
  doorLock.write(unlockPos); //moves the servo to the unlocked position
  delay(500);
  doorLock.detach(); //detaches the servo

  //this detach is nescessary because if the servo remains attached the key
  //will no longer work

  //sets the LED pins to output
  pinMode(ledPins[0],OUTPUT);
  pinMode(ledPins[1],OUTPUT);
  pinMode(ledPins[2],OUTPUT);
  pinMode(ledPins[3],OUTPUT);
}




void loop()
{
  delay(10);

  char key = keypad.getKey();
  if (key != NO_KEY)
  {
    if (key == 10)
    {
      key = 0; 
      //this is nescessary because the keypad library uses int 0 to represent
      //NO_KEY, so when the 0 key is pressed it returns 10, and this statement
      //changes the value of key to properly represent 0
    }
      if (key != '#' && key != '*') 
      {
        digitalWrite(ledPins[curr%4], HIGH); //turns on the lights one at a time
        entry[curr%4] = key; //writes the key press to the entry variable
        curr++; //incriments the cursor variable
      }
      if (entry[0] == code[0] && entry[1] == code[1] && entry[2] == code[2] && entry[3] == code[3]) //changes the lock position when the correct code is entered
      {
        door();              
      }
      else if (curr%4 == 0) //resets cursor and entry values if the wrong code is entered
      {
        curr = 0;
        entry[0] = 10;
        entry[1] = 10;
        entry[2] = 10;
        entry[3] = 10;
        
        blinkLights(50);
        
      }

      else if (key == '#' || key == '*') //clears the entry and resets the lights
      {
        curr = 0;
        digitalWrite(ledPins[0], LOW);
        digitalWrite(ledPins[1], LOW);
        digitalWrite(ledPins[2], LOW);
        digitalWrite(ledPins[3], LOW);
      }     
  }
  /*
  if (!(IRpin_PIN & (1 << IRpin))) //changes the lock position if the IR reciever gets input
  {
    door();
    delay(500
      );
  }
  */
}


//changes the lock position

void door()
{
  if (locked)
  {
    blinkLights(100);
    
    //moves servo
    doorLock.attach(servoPin);
    doorLock.write(unlockPos);
    delay(500);
    doorLock.detach();
    
    //sets the locked boolean to false and resets the entry/cursor variables
    locked = false;
    curr = 0;
    entry[0] = 10;
    entry[1] = 10;
    entry[2] = 10;
    entry[3] = 10;
  }
  else if (!locked)
  {
    blinkLights(200);
    
    //moves servo
    doorLock.attach(servoPin);
    doorLock.write(lockPos);
    delay(500);
    doorLock.detach();
    
    //sets the locked boolean to true and resets the entry/cursor variables
    locked = true;
    curr = 0;
    entry[0] = 10;
    entry[1] = 10;
    entry[2] = 10;
    entry[3] = 10;
  }
}


//blinks the lights 3 times at the speed given

void blinkLights(long blinkSpeed) 
{
  for (int i = 0; i < 3; i++)
  {
    digitalWrite(ledPins[0], HIGH);
    digitalWrite(ledPins[1], HIGH);
    digitalWrite(ledPins[2], HIGH);
    digitalWrite(ledPins[3], HIGH);
    delay(blinkSpeed);
    digitalWrite(ledPins[0], LOW);
    digitalWrite(ledPins[1], LOW);
    digitalWrite(ledPins[2], LOW);
    digitalWrite(ledPins[3], LOW);
    delay(blinkSpeed);
  }
}
